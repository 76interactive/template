Forza Digital - Concrete5 Template
==================================

### Gulp 
Task runner used to compile SASS assets

#### Installation
Required: Node.js 5 and up & NPM

```
$ npm install
```

#### Usage

```
$ gulp
```

#### Advanced usage
Specific tasks are taken as the first parameter.

```
$ gulp sass
```

### EditorConfig

Install editorConfig in your IDE.
[http://editorconfig.org/#download](http://editorconfig.org/#download)
