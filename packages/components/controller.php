<?php

namespace Concrete\Package\Components;

use Concrete\Core\Package\Package;
use Core;

defined('C5_EXECUTE') or die(_("Access Denied."));

class Controller extends Package {
  protected $pkgHandle  = 'components';
  protected $pkgVersion = '1.0.0';

  protected $appVersionRequired = '5.7.0';

  public function getPackageDescription() {
    return t('Adds injected Forza Components.');
  }

  public function getPackageName(){
    return t('Components');
  }
}
