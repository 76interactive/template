<?php $u = new User();?>

  <?php Loader::element('footer_required') ?>
  <?php if (!$u->isLoggedIn()):?>
  <script src="<?php echo $view->getThemePath() ?>/vendor/jquery/dist/jquery.min.js"></script>
  <?php endif;?>
  <script src="<?php echo $view->getThemePath() ?>/assets/js/app.js"></script>
</body>
</html>
