<?php $u = new User(); ?>
<!doctype html>
<html lang="nl">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
  <?php Loader::element('header_required') ?>
  <link href="<?php echo $view->getThemePath() ?>/assets/css/style.css" type="text/css" rel="stylesheet" />
</head>
<body<?php if ($u->isLoggedIn()):?> style="margin-top: 48px;"<?php endif; ?>>
