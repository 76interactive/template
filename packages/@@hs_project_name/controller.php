<?php

namespace Concrete\Package\@@cc_project_name;

use Concrete\Core\Package\Package;
use Concrete\Core\Page\Theme\Theme;
use Core;

defined('C5_EXECUTE') or die(_("Access Denied."));

class Controller extends Package {
  protected $pkgHandle  = '@@cc_project_name';
  protected $pkgVersion = '1.0.0';

  protected $appVersionRequired = '5.7.0';

  public function getPackageDescription() {
    return t('Adds a the @@sc_project_name theme.');
  }

  public function getPackageName(){
    return t('@@sc_project_name');
  }

  public function install() {
    $pkg = parent::install();
    Theme::add('@@cc_project_name', $pkg);
  }
}
